var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});

var app2 = new Vue({
    el: '#app2',
    data: {
        message: "A swishy title!"
    }
});

var app3 = new Vue({
    el: '#app3',
    data: {
        seen: true
    }
});

var app4 = new Vue({
    el: '#app4',
    data: {
        todos: [
            { text: "Flor de Cana" },
            { text: "Havana Especial" },
            { text: "Grog" }
        ]
    }
});

var app5 = new Vue({
    el: '#app5',
    data: {
        message: "A lovely message to reverse"
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('');
        }
    }
});

var app6 = new Vue({
    el: '#app6',
    data: {
        message: 'Some inputted text'
    }
});